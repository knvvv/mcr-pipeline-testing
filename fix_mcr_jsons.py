import json
import os
import glob

DIR_MASKS = [
    'mcr_*_spec',
    'mcr-vs-rdkit_*_spec',
    'mcr-vs-mtd_*_spec',
    'mcr-vs-crest_*_spec',
]


def fix_json(json_name: str) -> None:
    with open(json_name, 'r') as f:
        data = json.load(f)

    assert 'ringoCN' not in data['method'], f"This file is not fixed: {json_name}"
    print(f"{json_name} is OK")
    
    # assert 'ringoCN' in data['method'], f"This file is already fixed: {json_name}"
    # data['method'] = data['method'].replace('ringoCN', 'ringo')

    # with open(json_name, 'w') as f:
    #     json.dump(data, f)
    # print(f"Fixed '{json_name}'")


def main():
    dirs = [
        dirname
        for dir_mask in DIR_MASKS
        for dirname in glob.glob(os.path.join('.', dir_mask))
    ]

    json_names = [
        jsonname
        for dirname in dirs
        for jsonname in glob.glob(os.path.join(dirname, '*.json'))
    ]

    for item in json_names:
        fix_json(item)


if __name__ == "__main__":
    main()
